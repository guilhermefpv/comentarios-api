## O que será avaliado na sua solução?

* Automação da infra, provisionamento dos hosts (IaaS) [Provisionado com Terraform]

* Automação de setup e configuração dos hosts (IaC)[Provisionado com Terraform e Ansible]

* Pipeline de deploy automatizado [Gitlab.com]

* Monitoramento dos serviços e métricas da aplicação [Grafana.com]

## Fazer:

- colocar um step de test para validar funcionalidade da app
- rollback do container/image/codigo
- pipeline com tag
- pepeline deploy terraform na mesa pepeline da app
- Configura o TFC para backend dos tfstates mas poderia ser com S3 e DynamoDB servido como Backend

## Recursos de infraestrutura

- Incluir Autoscale
- Provisionar um DNS com Route53

## Monitoraca / Observabilidade

- CloudWatch (Implementado na infraestrutura)
- Grafana (Testando)
- Dynatrace (Testando)
- Datadog (Testando)


## Testes e escolha da solucao de infra

- O projeto foi testado em cluster EKS, porem a provisionamento demorou muito e os custos com recursos ficariam mais caros. Por isso decidi usar somente docker sem o provisionamento de instancias EC2.
- Testado com infraestrutura rodando instancias ec2 e o Jenkins com sonarquebe